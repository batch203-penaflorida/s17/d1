/* 
 Learn how to create functions in JavaScript. 
Code blocks I can re-use later in my code.
 Learn how to call functions in JavaScript.
 I know how to run and invoke functions to do certain tasks
 in my program.
 Learn the scope functions.
 I can recognize the scope of functions
 Learn about function naming conventions.
 I can create naming conventions of functions
*/
console.log("Hello World");

// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.

// The are also used to prevent repeating lines/blocks of codes that perform the same task/function.
// Function declarations
// function statement defines a function with the specified parameters.

/* 
   Syntax:
      function functionName(){
         code block(statement)
      }
      
      function keyword - use to define a javascript function.
      functionName - the function name is use so we are able to call/invoke are declare function

      function code block({;}) - the statements which comprise of the body of the function. this is where the code to be executed.
*/
function printName() {
  console.log("Print something.");
}
// function invocation
// It is common to use the term "Call a function" instead of "Invoke a function"/
// Let's invoke/call the function that was declared.

printName();

//declaredFunction(); // results in an error, much like variables, we cannot invoke a function that we have not define yet.

// function declaration global scope
// can be called in any line of function declaration.

// hoisting

// declared functions can be hoisted. As long as th function has been defined.

// Hoisting is JavaScript's behavior for certain variables(var) and functions to run or use them before the declaration.

function declaredFunction() {
  console.log("Hello World");
}

declaredFunction();
// function declaration
/* Can be created through functoin declaration by using the function keyword and adding a function name */

// 2. Function expression is an anonymous function assigned to the variableFunction.
// Anonymous function - a function without a name.

//variableFunction(); //error - function expressions, being stored in a let or const variable. Cannot be hoisted.

let variableFunction = function () {
  console.log("Hello Again!");
};
variableFunction();

let funcExpression = function funcName() {
  console.log("Hello from the other side.");
};
// funcExpression = declaredFunction();
funcExpression();
// funcName() result to an not defined function.
declaredFunction();
// we can also create a function expression of a named function.
declaredFunction = function () {
  console.log("Updated declared function");
};
declaredFunction();
// funcName();
functionExpression = function () {
  console.log("Updated funcExpression");
};
functionExpression();
const constantFunction = function () {
  console.log("Initialized with const");
};
constantFunction();
// We cannot re-assign a function expression.
// constantFunction = function () {
//   console.log("Initialized with const");
// };
// constantFunction();

// function scope
// variables declared inside a {} block can only be access locally
// local and block scope only works with let and const.
{
  let localVar = "Armando Perez";
  console.log(localVar);
}
let globalVar = "Mr. Worldwide";
console.log(globalVar);

// function scopes
// javascript has function scopes: Each function creates a new scope.
// variables defined inside a function are not accessible/visible outside the function.
// variables declared with var, let, and const are quite similar when declared inside a function.

function showNames() {
  var functionVar = "Joe";
  const functionConst = "John";
  let functionLet = "Jane";
  console.log(functionVar);
  console.log(functionConst);
  console.log(functionLet);
}

showNames();
//Error - This are function scope variable and cannot be accessed outside the function they were declared in.
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions
// You can create another function inside a function.
// This is called a nested function.
function myNewFunction() {
  let name = "Jane";
  function nestedFunction() {
    let nestedName = "John";
    console.log(name);
  }
  nestedFunction();
}
myNewFunction();

// Function and Global Scope Variables

// Global Scope variable
let globalName = "Alexandro";

function myNewFunction2() {
  let nameInside = "Renz";
  console.log(globalName);
}
myNewFunction2();
// Using alert();

/* alert() allows us to show a small window at the top of our borwser page to show informatino to our users.
It allows us to show a short dialog or instructions to our users. The page will wait until the user dismisses the dialog.

*/

// Jo
// Notes on the use of alert();
// Show only an alert() for show dialogs/messages to the user.

// Using prompt()
// prompt() allows us to show small window at the top of the browser to gather user input.
// The input form the prompt() will be returned as a "String"
// syntax:

// let variableName = prompt("<dialogInString>");

// let samplePropmt = prompt("Enter your full Name");
// // console.log(typeof samplePropmt);
// console.log("Hello " + samplePropmt);

// let sampleNullPrompt = prompt("Don't enter anything.");

// console.log(sampleNullPrompt);
